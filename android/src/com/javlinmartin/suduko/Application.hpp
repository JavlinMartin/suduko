//----------------------------------------------------------------------------------------------------------------------
// File:         Application.hpp
// Created on:   2016-11-12
// Author:       Mike Martin
// Description:  An Android application
//----------------------------------------------------------------------------------------------------------------------

#ifndef COM_JAVLINMARTIN_SUDUKO_APPLICATION_HPP_
#define COM_JAVLINMARTIN_SUDUKO_APPLICATION_HPP_

#include "ApplicationBase.hpp"

namespace javlinmartin {
namespace suduko {

class Application : public ApplicationBase {
    Q_OBJECT
public:
    Application(int &argc, char **argv);
    static Application *getInstance();

protected:

private:

};

} // namespace suduko
} // namespace javlinmartin

#endif // XNINJAS_APPLICATION_HPP_
