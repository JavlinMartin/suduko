//----------------------------------------------------------------------------------------------------------------------
// File:         Application.cpp
// Created on:   2016-11-12
// Author:       Mike Martin
// Description:  An Android application
//----------------------------------------------------------------------------------------------------------------------

#include "Application.hpp"

namespace javlinmartin {
namespace suduko {

Application::Application(int &argc, char **argv) :
    ApplicationBase(argc, argv) {
    setApplicationName("Android Suduko");
    init();
}

Application *Application::getInstance() {
    return static_cast<Application *>(QCoreApplication::instance());
}

} // namespace system
} // namespace xninjas
