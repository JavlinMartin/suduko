#include <QApplication>
#include <QQmlApplicationEngine>
#include "Application.hpp"

int main(int argc, char *argv[])
{
    javlinmartin::suduko::Application app(argc, argv);
    return app.exec();
}
