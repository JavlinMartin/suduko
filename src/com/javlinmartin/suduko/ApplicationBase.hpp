//----------------------------------------------------------------------------------------------------------------------
// File:         ApplicationBase.hpp
// Created on:   2016-11-12
// Author:       Mike Martin
// Description:  Base class for the application
//----------------------------------------------------------------------------------------------------------------------

#ifndef COM_JAVLINMARTIN_SUDUKO_APPLICATIONBASE_HPP_
#define COM_JAVLINMARTIN_SUDUKO_APPLICATIONBASE_HPP_

#include <QQmlApplicationEngine>
#include <QTranslator>
#include <QApplication>

namespace javlinmartin {
namespace suduko {

class ApplicationBase : public QApplication {
    Q_OBJECT
public:

    explicit ApplicationBase(int &argc, char **argv);
    virtual void registerQmlTypes();
    virtual void setContextProperties(QQmlContext *rootContext);


protected:
    void init();

private:
    QQmlApplicationEngine engine_;

};

} // namespace suduko
} // namespace javlinmartin

#endif // COM_JAVLINMARTIN_SUDUKO_APPLICATIONBASE_HPP_
