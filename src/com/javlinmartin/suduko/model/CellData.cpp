//-----------------------------------------------------------------------------
// File:         CellData.cpp
// Created on:   2016-11-12
// Author:       M. Martin
// Copyright:    Awesome @2016
// Description:  Data associated with each cell from the grid.
//               See the header for info on each property
//-----------------------------------------------------------------------------

#include <QDebug>
#include <QDateTime>
#include <QQmlEngine>
#include <QThread>
#include <QApplication>

#include "model/CellData.hpp"

namespace javlinmartin {
namespace suduko {
namespace model {

CellData::CellData(QObject *parent) :
    QObject(parent),
    givenValue_(0),
    tentativeValue_(0),
    quadrant_(0),
    row_(0),
    column_(0),
    preventUpdates_(false){

    QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
}

CellData::CellData(CellData &info) :
    QObject(info.parent()),
    givenValue_(info.givenValue()),
    tentativeValue_(info.tentativeValue()),
    posibleValues_(info.posibleValues()),
    quadrant_(info.quadrant()),
    row_(info.row()),
    column_(info.column()){

}

CellData::~CellData() {

}

//////////////////////////////////////////////////////////////////////////
///  Setters
//////////////////////////////////////////////////////////////////////////

    void CellData::setGivenValue(int i_value){
        givenValue_ = i_value;
        emit givenValueChanged();
    }

    void CellData::setPosibleValues(QList<int> i_values){
        posibleValues_ = i_values;
    }

    void CellData::setQuadrant(int i_quadrant){
        quadrant_ = i_quadrant;
    }

    void CellData::setRow(int i_row){
        row_ = i_row;
    }

    void CellData::setColumn(int i_column){
        column_ = i_column;
    }

    void CellData::setTentativeValue(int i_value){
        tentativeValue_ = i_value;
        if(!preventUpdates_){
            emit tentativeValueChanged();
        }
    }

    void CellData::setPreventUpdates(bool i_prevent){
        preventUpdates_ = i_prevent;
        if(!preventUpdates_){
            emit tentativeValueChanged();
        }
    }

//////////////////////////////////////////////////////////////////////////
///  Getters
//////////////////////////////////////////////////////////////////////////

    int CellData::givenValue(){
        return givenValue_;
    }

    QList<int>& CellData::posibleValues(){
        return posibleValues_;
    }

    int CellData::quadrant(){
        return quadrant_;
    }

    int CellData::row(){
        return row_;
    }

    int CellData::column(){
        return column_;
    }

    int CellData::tentativeValue(){
        return tentativeValue_;
    }

} // namespace model
} // namespace suduko
} // namespace javlinmartin
