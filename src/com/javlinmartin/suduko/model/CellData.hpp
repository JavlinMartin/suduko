//----------------------------------------------------------------------------------------------------------------------
// File:         CellData.hpp
// Created on:   2016-11-12
// Author:       Mike Martin
// Description:  Data associated with each cell from the grid.
//----------------------------------------------------------------------------------------------------------------------

#ifndef COM_JAVLINMARTIN_SUDUKO_SQUAREDATA_HPP_
#define COM_JAVLINMARTIN_SUDUKO_SQUAREDATA_HPP_

#include <QVariant>
#include <QList>


namespace javlinmartin {
namespace suduko {
namespace model {

class CellData : public QObject {
    Q_OBJECT

public:
    explicit CellData(QObject *parent = 0);
    CellData(CellData &data);
    virtual ~CellData();

     /////

    Q_PROPERTY(int givenValue READ givenValue WRITE setGivenValue NOTIFY givenValueChanged())
    Q_PROPERTY(int tentativeValue READ tentativeValue WRITE setTentativeValue NOTIFY tentativeValueChanged())

    void setGivenValue(int i_value);
    void setPosibleValues(QList<int> i_values);
    void setQuadrant(int i_quadrant);
    void setRow(int i_row);
    void setColumn(int i_column);
    void setTentativeValue(int i_value);
    void setPreventUpdates(bool i_prevent);

    int givenValue();
    QList<int>& posibleValues();
    int quadrant();
    int row();
    int column();
    int tentativeValue();


//////////////////////////////////////////////////////////////////////////
///  Signals
//////////////////////////////////////////////////////////////////////////

signals:
    void givenValueChanged();
    void tentativeValueChanged();

//////////////////////////////////////////////////////////////////////////
///  Signals
//////////////////////////////////////////////////////////////////////////

private:

    // The value within a cell that has been given to the user (immutable)
    int givenValue_;

    // The value within a cell that may be the correct one
    int tentativeValue_;

    // A list of possible numbers determined by looking at other items in the row/column/quadrant
    QList<int> posibleValues_;

    // Quadrants are defined like the diagrame below
    //    +-----+-----+-----+
    //    |     |     |     |
    //    |  0  |  1  |  2  |
    //    |     |     |     |
    //    +-----+-----+-----+
    //    |     |     |     |
    //    |  3  |  4  |  5  |
    //    |     |     |     |
    //    +-----+-----+-----+
    //    |     |     |     |
    //    |  6  |  7  |  8  |
    //    |     |     |     |
    //    +-----+-----+-----+
    int quadrant_;

    // The row this cell belongs to
    int row_;

    // The column this cell belongs to
    int column_;

    // During intensive operations this flag prevents the ui from updating the tentative value changes
    // This was needed because while solving the puzzle the UI thread was slowing down due to the UI constantly upating
    bool preventUpdates_;
};

} // namespace model
} // namespace suduko
} // namespace javlinmartin

#endif // COM_JAVLINMARTIN_SUDUKO_SQUAREDATA_HPP_
