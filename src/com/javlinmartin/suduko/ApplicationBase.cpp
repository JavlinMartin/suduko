//----------------------------------------------------------------------------------------------------------------------
// File:         ApplicationBase.cpp
// Created on:   2016-11-12
// Author:       Mike Martin
// Description:  Base class for the application
//----------------------------------------------------------------------------------------------------------------------

#include <QtGlobal>
#include <QObject>
#include <QLocale>
#include <QQmlContext>
#include <QDir>
#include <QMap>
#include <QDebug>

#include "ApplicationBase.hpp"
#include "commands/GetInitialStateCommand.hpp"
#include "commands/SolvePuzzleCommand.hpp"
#include "commands/SavePuzzleCommand.hpp"
#include "model/CellData.hpp"


namespace javlinmartin {
namespace suduko {

ApplicationBase::ApplicationBase(int &argc, char **argv) :
    QApplication(argc, argv) {
}

void ApplicationBase::init() {
    setOrganizationDomain("javlinmartin.com");
    setOrganizationName("Awesome");

    registerQmlTypes();
    setContextProperties(engine_.rootContext());

    engine_.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
}

void ApplicationBase::registerQmlTypes() {

    // You need to registerType when you want to use the object within QML.  
   qmlRegisterType<commands::GetInitialStateCommand>("commands", 1, 0, "GetInitialStateCommand");
   qmlRegisterType<commands::SolvePuzzleCommand>("commands", 1, 0, "SolvePuzzleCommand");
   qmlRegisterType<commands::SavePuzzleCommand>("commands", 1, 0, "SavePuzzleCommand");
   qmlRegisterType<model::CellData>("model", 1, 0, "CellData");
}

void ApplicationBase::setContextProperties(QQmlContext *context) {
    context->setContextProperty("s_app", QApplication::instance());
}

} // namespace suduko
} // namespace javlinmartin
