//----------------------------------------------------------------------------------------------------------------------
// File:         Command.hpp
// Created on:   2016-11-12
// Author:       Mike Martin
// Copyright:    Awesome @2016
// Description:  The base class for all Commands
//-----------------------------------------------------------------------------

#ifndef COM_JAVLINMARTIN_SUDUKO_COMMAND_HPP_
#define COM_JAVLINMARTIN_SUDUKO_COMMAND_HPP_

#include <QVariant>

namespace javlinmartin {
namespace suduko {
namespace commands {

class Command : public QObject {
    Q_OBJECT

public:
    explicit Command(QObject *parent = 0);
    Command(const Command &command);
	virtual ~Command();

public slots:

    Q_INVOKABLE virtual void execute(QVariant data = QVariant()) = 0;

protected:


private slots:


signals:

    Q_SIGNAL void completed();
    Q_SIGNAL void completed(QVariant data);

private:

};

} // namespace commands
} // namespace suduko
} // namespace javlinmartin

#endif // COM_JAVLINMARTIN_SUDUKO_COMMAND_HPP_
