//----------------------------------------------------------------------------------------------------------------------
// File:         SolvePuzzleCommand.hpp
// Created on:   2016-11-12
// Author:       Mike Martin
// Description:  Here is where all the magic happens when attempting to solve a puzzle.
//               Input: All 81 CellData objects
//               Output: True if puzzle is solved, false if it's not
//----------------------------------------------------------------------------------------------------------------------

#ifndef COM_JAVLINMARTIN_SUDUKO_SOLVEPUZZLECOMMAND_HPP_
#define COM_JAVLINMARTIN_SUDUKO_SOLVEPUZZLECOMMAND_HPP_

#include <QVariant>

#include "commands/Command.hpp"
#include "model/CellData.hpp"

namespace javlinmartin {
namespace suduko {
namespace commands {

class SolvePuzzleCommand: public Command {
    Q_OBJECT

public:
    explicit SolvePuzzleCommand(QObject *parent = 0);
    virtual ~SolvePuzzleCommand();

    Q_INVOKABLE void execute(QVariant variant = QVariant());

public slots:

private slots:

    void onThreadReady();

private:

    bool bruteForce(QList<model::CellData*> items);
    void cleanupList(QList<model::CellData*>& items);
    bool purgeList(QList<model::CellData*>& items);
    void setPossibleValues(const QList<model::CellData*>&);
    void setPossibleValues(model::CellData &data);
    bool isSolved();

    // All 81 model::CellData items
    QList<model::CellData*>allItems;

    // Multimap of CellData organized by quadrant
    QMap<int, model::CellData*>quadrantMap;

    // Multimap of CellData organized by column
    QMap<int, model::CellData*>columnMap;

    // Multimap of CellData organized by row
    QMap<int, model::CellData*>rowMap;

};

} // namespace commands
} // namespace suduko
} // namespace javlinmartin

#endif // COM_JAVLINMARTIN_SUDUKO_SOLVEPUZZLECOMMAND_HPP_
