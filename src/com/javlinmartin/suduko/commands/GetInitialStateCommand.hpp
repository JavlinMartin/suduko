//-----------------------------------------------------------------------------
// File:         GetInitialStateCommand.cpp
// Created on:   2016-11-13
// Author:       M. Martin
// Copyright:    Awesome @2016
// Description:  Get's the initial state of the board.
//               This command is also called when the board is reset
//-----------------------------------------------------------------------------

#ifndef COM_JAVLINMARTIN_SUDUKO_GETINITIALSTATECOMMAND_HPP_
#define COM_JAVLINMARTIN_SUDUKO_GETINITIALSTATECOMMAND_HPP_

#include <QVariant>

#include "commands/Command.hpp"

namespace javlinmartin {
namespace suduko {
namespace commands {

class GetInitialStateCommand: public Command {
    Q_OBJECT

public:
    explicit GetInitialStateCommand(QObject *parent = 0);
    virtual ~GetInitialStateCommand();

    Q_INVOKABLE void execute(QVariant variant = QVariant());

public slots:

private:

};

} // namespace commands
} // namespace suduko
} // namespace javlinmartin

#endif // COM_JAVLINMARTIN_SUDUKO_GETINITIALSTATECOMMAND_HPP_
