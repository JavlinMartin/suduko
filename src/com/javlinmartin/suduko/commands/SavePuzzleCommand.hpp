//-----------------------------------------------------------------------------
// File:         SavePuzzleCommand.cpp
// Created on:   2016-11-13
// Author:       M. Martin
// Copyright:    Awesome @2016
// Description:  Save's the current state of the board to QSettings
//               Input: All 81 CellData objects
//-----------------------------------------------------------------------------

#ifndef COM_JAVLINMARTIN_SUDUKO_SAVEPUZZLECOMMAND_HPP_
#define COM_JAVLINMARTIN_SUDUKO_SAVEPUZZLECOMMAND_HPP_

#include <QVariant>

#include "commands/Command.hpp"
#include "model/CellData.hpp"

namespace javlinmartin {
namespace suduko {
namespace commands {

class SavePuzzleCommand: public Command {
    Q_OBJECT

public:
    explicit SavePuzzleCommand(QObject *parent = 0);
    virtual ~SavePuzzleCommand();

    Q_INVOKABLE void execute(QVariant variant = QVariant());

    static const char *s_givenValueQSettingsType;
    static const char *s_tentativeValueQSettingsType;

public slots:

private:


};

} // namespace commands
} // namespace suduko
} // namespace javlinmartin

#endif // COM_JAVLINMARTIN_SUDUKO_SAVEPUZZLECOMMAND_HPP_
