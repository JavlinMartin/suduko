//-----------------------------------------------------------------------------
// File:         SolvePuzzleCommand.cpp
// Created on:   2016-11-12
// Author:       M. Martin
// Copyright:    Awesome @2016
// Description:  Here is where all the magic happens when attempting to solve a puzzle.
//               Input: All 81 CellData objects
//               Output: True if puzzle is solved, false if it's not
//-----------------------------------------------------------------------------

#include <QDebug>
#include <QtCore/qmath.h>
#include <QThread>
#include <QApplication>
#include <QQmlEngine>

#include "commands/SolvePuzzleCommand.hpp"
#include "commands/Command.hpp"


bool possibleValueSort(javlinmartin::suduko::model::CellData * a, javlinmartin::suduko::model::CellData * b)
{
    return a->posibleValues().length() < b->posibleValues().length();
}

namespace javlinmartin {
namespace suduko {
namespace commands {

SolvePuzzleCommand::SolvePuzzleCommand(QObject *parent) :
    Command(parent) {

}

SolvePuzzleCommand::~SolvePuzzleCommand() {

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Sets up each CellData with their respective row, column and quadrant data which is used when solving the puzzle.
///        Passes this command to a new thread as this process can be quite intensive.
/// \param variant A list of all the CellData objects
///
void SolvePuzzleCommand::execute(QVariant variant) {

    // Here is a list of 81 CellData object's organized on the board from left to right, then top to bottom
    //                                          (Like words on a page if you were reading a book)
    QVariantList list = variant.toList();

    // Typically this command would get deleted and re-created each time,
    // but just in case let's start with a clean slate
    allItems.clear();
    quadrantMap.clear();
    columnMap.clear();
    rowMap.clear();

    for(int i=0;i<list.length();i++){

        // Get the CellData object from the QVariantList
        model::CellData *data = (model::CellData *)list.at(i).value<QObject*>();

        // Stop updates to the UI in order to prevent Lagging on the UI thread
        data->setPreventUpdates(true);

        // Assign a column and row to each CellData object
        data->setColumn(i % 9);
        data->setRow(qFloor(i / 9));

        // Assign a quadrant to each CellData object
        // Quadrants are defined like the diagrame below
        //
        //    +-----+-----+-----+
        //    |     |     |     |
        //    |  0  |  1  |  2  |
        //    |     |     |     |
        //    +-----+-----+-----+
        //    |     |     |     |
        //    |  3  |  4  |  5  |
        //    |     |     |     |
        //    +-----+-----+-----+
        //    |     |     |     |
        //    |  6  |  7  |  8  |
        //    |     |     |     |
        //    +-----+-----+-----+

        int quadCol = qFloor(data->column() / 3);
        int quadRow = qFloor(data->row() / 3);
        data->setQuadrant((quadRow * 3) + quadCol);

        allItems.append(data);
        quadrantMap.insertMulti(data->quadrant(), data);
        columnMap.insertMulti(data->column(), data);
        rowMap.insertMulti(data->row(), data);
    }


    // Since this is a pretty heavy opperation let's run it on another thread
    QThread* thread = new QThread();
    this->setParent(NULL);
    this->moveToThread(thread);
    connect(thread, SIGNAL(started()), this, SLOT(onThreadReady()));
    connect(this, SIGNAL(completed()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Tries to solve the puzzle in the new thread
///
void SolvePuzzleCommand::onThreadReady() {

    // Start all the recursive magic
    bool success = bruteForce(allItems);

    qDebug() << "Puzzle is solved: " << success;

    // Continue updates on the UI thread.
    // trigger the UI to update - setPreventUpdates emites tentativeValueChanged
    foreach(model::CellData* data, allItems){
        data->setPreventUpdates(false);
    }

    this->moveToThread(QApplication::instance()->thread());

    // Fire off the complete signal to be caught by the QML
    emit completed(success);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief This is a method that is called recursively to try and solve the puzzle.
/// \param items A subset of CellData items
/// \return Return true if the puzzle is solved. Oh yeah!
///
bool SolvePuzzleCommand::bruteForce(QList<model::CellData*> items) {

    // Organizes, fills in and filters the list so we're working with just the items we care about
    cleanupList(items);

    // See if the puzzle is solved
    bool solved = isSolved();
    if(solved){
        qDebug() << "It's solved!";
        return true;
    }

    // If there 0 or 1 items in the list then return false
    // (the last tentative number we tried didn't work as the puzzle is not solved)
    if(items.length() <= 1){
        return false;
    }

    // Take the CellData item with the lowest number of possible values
    model::CellData *data = items.takeFirst();

    // qDebug() << "CHECK CHILDREN :: [" << data->row() << "][" << data->column() << "] CHILDREN:" << items.length() << " : SIBLINGS:" << data->posibleValues().length() << data->posibleValues();

    bool success = false;
    int numValues = data->posibleValues().length();
    // Loops over all the possible values
    for(int i = 0; i < numValues; i++ ) {

        int possibleValue = data->posibleValues().at(i);
        // Set this possible value as a tentative value in the cell
        data->setTentativeValue(possibleValue);

        // Let's see if we can solve the puzzle with that tentative value set
        success = bruteForce(items);
        if(success){
            break;
        }

        // Since we didn't solve the puzzle make sure to reset all tentative values to 0
        foreach(model::CellData *data, items){
            data->setTentativeValue(0);
        }
    }

    return success;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief This method prepares each item on the board by doing 3 things
///        1. Populates each CellData item with it's possible values. ie. 1,2,3 etc.
///        2. Sorts the list of CellData items from least number of possible values to most
///        3. Cleans up the list by setting the tentative value (if one possible value exists), or removing it if none exist
///        It will continue to loop through each item until no more tentative values have been set
///        This will ensure the list is at the best state we can get it without making a guess
/// \param items A reference to a subset of CellData items
///
void SolvePuzzleCommand::cleanupList(QList<model::CellData*>& items) {

    bool altered = false;
    do {
        setPossibleValues(items);

        // Sort by the number of possible values
        std::sort(items.begin(), items.end(), possibleValueSort);

        // Remove any items from the list that have 0 or 1 possible values
        // Also set a tentative value to any item that has 1 possible value
        altered = purgeList(items);

    } while(altered);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Cleans up the list by setting the tentative value (if one possible value exists), or removing it if none exist
/// \param items A reference to a subset of CellData items
/// \return Return true if we have set a new tentative value to a CellData item
///
bool SolvePuzzleCommand::purgeList(QList<model::CellData*>& items) {

    foreach(model::CellData *data, items){
       if(data->posibleValues().length() == 0){
           items.removeOne(data);
       }else if(data->posibleValues().length() == 1){
           int onlyValue = data->posibleValues().at(0);
           data->setTentativeValue(onlyValue);
           items.removeOne(data);
           return true;
       }
    }

    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Populates each CellData item with it's possible values. ie. 1,2,3 etc.
/// \param items A subset of CellData items
///
void SolvePuzzleCommand::setPossibleValues(const QList<model::CellData*>& items) {

    foreach(model::CellData *data, items){
        data->setTentativeValue(0);
        setPossibleValues(*data);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Populates a CellData item with it's possible values. ie. 1,2,3 etc.
/// \param data The CellData item
///
void SolvePuzzleCommand::setPossibleValues(model::CellData &data) {

    QList<int> possibleValues;
    if(data.givenValue() == 0 && data.tentativeValue() == 0){

        possibleValues << 1 << 2 << 3 << 4 << 5 << 6 << 7 << 8 << 9;

        // Remove any possible numbers that are in the same quadrant
        QList<model::CellData*>quadrantList = quadrantMap.values(data.quadrant());
        foreach(model::CellData* quadrantItem, quadrantList){
            possibleValues.removeOne(quadrantItem->givenValue());
            possibleValues.removeOne(quadrantItem->tentativeValue());
        }

        // Remove any possible numbers that are in the same row
        QList<model::CellData*>rowList = rowMap.values(data.row());
        foreach(model::CellData* rowItem, rowList){
            possibleValues.removeOne(rowItem->givenValue());
            possibleValues.removeOne(rowItem->tentativeValue());
        }

        // Remove any possible numbers that are in the same column
        QList<model::CellData*>columnList = columnMap.values(data.column());
        foreach(model::CellData* columnItem, columnList){
            possibleValues.removeOne(columnItem->givenValue());
            possibleValues.removeOne(columnItem->tentativeValue());
        }
    }

    data.setPosibleValues(possibleValues);

    // qDebug() << "possibleValues :: " << data.row() << " : " << data.column() << " : " << possibleValues;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Determine if the board is solved by seeing if every square has a selected value or tentative value
/// \return Returns true if the board is solved
///
bool SolvePuzzleCommand::isSolved() {

    foreach(model::CellData *data, allItems){

        if(data->givenValue() == 0 && data->tentativeValue() == 0){
            return false;
        }
    }
    return true;
}



} // namespace commands
} // namespace suduko
} // namespace javlinmartin
