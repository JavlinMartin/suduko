//-----------------------------------------------------------------------------
// File:         Command.cpp
// Created on:   2016-11-12
// Author:       M. Martin
// Copyright:    Awesome @2016
// Description:  The base class for all Commands
//-----------------------------------------------------------------------------

#include <QDebug>
#include <QDateTime>
#include <QQmlEngine>
#include <QThread>
#include <QApplication>

#include "commands/Command.hpp"

namespace javlinmartin {
namespace suduko {
namespace commands {

Command::Command(QObject *parent) :
    QObject(parent) {

    QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
}

Command::Command(const Command &command) :
    QObject(command.parent()) {

}

Command::~Command() {
}

} // namespace commands
} // namespace suduko
} // namespace javlinmartin
