//-----------------------------------------------------------------------------
// File:         SavePuzzleCommand.cpp
// Created on:   2016-11-13
// Author:       M. Martin
// Copyright:    Awesome @2016
// Description:  Saves the current state of the board to QSettings
//               Input: All 81 CellData objects
//               Output: None
//-----------------------------------------------------------------------------

#include <QDebug>
#include <QApplication>
#include <QSettings>
#include <QtCore/qmath.h>

#include "commands/SavePuzzleCommand.hpp"
#include "commands/Command.hpp"

namespace javlinmartin {
namespace suduko {
namespace commands {

const char *SavePuzzleCommand::s_givenValueQSettingsType = "givenValueList";
const char *SavePuzzleCommand::s_tentativeValueQSettingsType = "tentativeValueList";

SavePuzzleCommand::SavePuzzleCommand(QObject *parent) :
    Command(parent) {

}

SavePuzzleCommand::~SavePuzzleCommand() {

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Saves all tentative and given values to QSettings
/// \param variant The list of all CellData objects
///
void SavePuzzleCommand::execute(QVariant variant) {

    QVariantList list = variant.toList();

    QVariantList givenValueList;
    QVariantList tentativeValueList;

    // Loop over each item and populate simple QVariantList's so we can save it to QSettings
    foreach(QVariant variant, list){
        model::CellData *data = (model::CellData *)variant.value<QObject*>();
        givenValueList.append(data->givenValue());
        tentativeValueList.append(data->tentativeValue());
    }

    QSettings settings(QApplication::applicationName());
    settings.setValue(s_givenValueQSettingsType, givenValueList);
    settings.setValue(s_tentativeValueQSettingsType, tentativeValueList);

    // Fire off the complete signal to be caught by the QML
    emit completed();
}


} // namespace commands
} // namespace suduko
} // namespace javlinmartin
