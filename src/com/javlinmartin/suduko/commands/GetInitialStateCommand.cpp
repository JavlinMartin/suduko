//-----------------------------------------------------------------------------
// File:         GetInitialStateCommand.cpp
// Created on:   2016-11-13
// Author:       M. Martin
// Copyright:    Awesome @2016
// Description:  Get's the initial state of the board.
//               Input: None
//               Output: List of all 81 CellData objects
//-----------------------------------------------------------------------------

#include <QDebug>
#include <QSettings>
#include <QApplication>
#include <QtCore/qmath.h>

#include "commands/GetInitialStateCommand.hpp"
#include "commands/SavePuzzleCommand.hpp"
#include "commands/Command.hpp"
#include "model/CellData.hpp"

namespace javlinmartin {
namespace suduko {
namespace commands {

GetInitialStateCommand::GetInitialStateCommand(QObject *parent) :
    Command(parent) {

}

GetInitialStateCommand::~GetInitialStateCommand() {

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Saves all tentative and given values to QSettings
/// \param variant The list of all CellData objects
///
void GetInitialStateCommand::execute(QVariant variant) {

    Q_UNUSED(variant);

    QVariantList list;

    // Get any saved given and tentative values
    QSettings settings(QApplication::applicationName());
    QVariantList savedGivenValues = settings.value(SavePuzzleCommand::s_givenValueQSettingsType).toList();
    QVariantList savedTentativeValues = settings.value(SavePuzzleCommand::s_tentativeValueQSettingsType).toList();

    // Here are initial values incase none have been saved
    QList<int> defaultGivenValues;
    defaultGivenValues << 8 << 5 << 6 << 0 << 1 << 4 << 7 << 3 << 0;
    defaultGivenValues << 0 << 9 << 0 << 0 << 0 << 0 << 0 << 0 << 0;
    defaultGivenValues << 2 << 4 << 0 << 0 << 0 << 0 << 1 << 6 << 0;
    defaultGivenValues << 0 << 6 << 2 << 0 << 5 << 9 << 3 << 0 << 0;
    defaultGivenValues << 0 << 3 << 1 << 8 << 0 << 2 << 4 << 5 << 0;
    defaultGivenValues << 0 << 0 << 5 << 3 << 4 << 0 << 9 << 2 << 0;
    defaultGivenValues << 0 << 2 << 4 << 0 << 0 << 0 << 0 << 7 << 3;
    defaultGivenValues << 0 << 0 << 0 << 0 << 0 << 0 << 0 << 1 << 0;
    defaultGivenValues << 0 << 1 << 8 << 6 << 3 << 0 << 2 << 9 << 4;

    for(int i=0;i<defaultGivenValues.length();i++){

        model::CellData *data = new model::CellData();
        if(savedGivenValues.length() > 0 || savedTentativeValues.length() > 0){
            data->setGivenValue(savedGivenValues.at(i).toInt());
            data->setTentativeValue(savedTentativeValues.at(i).toInt());
        }else{
            data->setGivenValue(defaultGivenValues.at(i));
        }

        list.append(QVariant::fromValue(data));
    }

    // Fire off the complete signal to be caught by the QML
    emit completed(list);
}

} // namespace commands
} // namespace suduko
} // namespace javlinmartin
