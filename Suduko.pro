TEMPLATE = app

QT += qml quick core gui widgets opengl

CONFIG += c++11

mac {
    message("Using mac settings.");
    PLATFORM_PREFIX = android
}

ios {
    message("Using ios settings.");
    PLATFORM_PREFIX = ios
}

android {
    message("Using android settings.");

    QT += androidextras

    PLATFORM_PREFIX = android
    PLATFORM_ARCH = android-armv7
}

windows {
    message("Using windows settings.");

    PLATFORM_PREFIX = windows
}

DEPENDPATH += src/com/javlinmartin/suduko
DEPENDPATH += $$PWD/$$PLATFORM_PREFIX/src/com/javlinmartin/suduko

INCLUDEPATH += src/com/javlinmartin/suduko
INCLUDEPATH += $$PWD/$$PLATFORM_PREFIX/src/com/javlinmartin/suduko

SOURCES += \
    main.cpp \
    src/com/javlinmartin/suduko/ApplicationBase.cpp \
    $$PLATFORM_PREFIX/src/com/javlinmartin/suduko/Application.cpp \
    src/com/javlinmartin/suduko/commands/Command.cpp \
    src/com/javlinmartin/suduko/commands/GetInitialStateCommand.cpp \
    src/com/javlinmartin/suduko/commands/SolvePuzzleCommand.cpp \
    src/com/javlinmartin/suduko/commands/SavePuzzleCommand.cpp \
    src/com/javlinmartin/suduko/model/CellData.cpp \

HEADERS += \
    src/com/javlinmartin/suduko/ApplicationBase.hpp \
    $$PLATFORM_PREFIX/src/com/javlinmartin/suduko/Application.hpp \
    src/com/javlinmartin/suduko/commands/Command.hpp \
    src/com/javlinmartin/suduko/commands/GetInitialStateCommand.hpp \
    src/com/javlinmartin/suduko/commands/SolvePuzzleCommand.hpp \
    src/com/javlinmartin/suduko/commands/SavePuzzleCommand.hpp \
    src/com/javlinmartin/suduko/model/CellData.hpp \

RESOURCES += assets/qml/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
