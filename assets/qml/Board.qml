import QtQuick 2.0
import commands 1.0
import model 1.0
import QtQuick.Layouts 1.3

Rectangle {

    id: boardRect
    property bool editMode: false
    border.color: "black"
    border.width: 2
    property alias model: grid.model

    signal selectNumber(variant itemData);

    GridView {
        id: grid
        anchors.fill: parent
        cellWidth: parent.width / 9;
        cellHeight: cellWidth
        delegate: Cell {
            width: parent.width / 9;
            height: width
            itemData: grid.model[index]
            onSelected: {
                selectNumber(itemData);
            }
        }
        focus: true
        flickableDirection: Flickable.AutoFlickIfNeeded
    }

    // Quadrant dividers
    Rectangle {
        x: parent.width / 3
        width: 1
        height: parent.height
        color: "black"
    }
    // Quadrant dividers
    Rectangle {
        x: parent.width / 3 * 2
        width: 1
        height: parent.height
        color: "black"
    }
    // Quadrant dividers
    Rectangle {
        y: parent.height / 3
        width: parent.width
        height: 1
        color: "black"
    }
    // Quadrant dividers
    Rectangle {
        y: parent.height / 3 * 2
        width: parent.width
        height: 1
        color: "black"
    }

}
