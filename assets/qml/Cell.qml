import QtQuick 2.0
import model 1.0

Item {

    id: rootItem
    width: parent.height
    height: width

    property CellData itemData;
    signal selected(variant itemData);

    Component.onCompleted: {
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            rootItem.selected(itemData);
        }
    }
    // Given value text
    Text {
        text: itemData.givenValue > 0 ? itemData.givenValue : ""
        font.bold: true
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: parent.height / 2
    }

    // Tentative value text
    Text {
        text: itemData.tentativeValue > 0 ? itemData.tentativeValue : ""
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: parent.height / 2
        color: "grey"
        visible: itemData.givenValue == 0
    }

    Rectangle {
        anchors.right: parent.right
        width: 1
        height: parent.height
        color: "grey"
    }

    Rectangle {
        anchors.bottom: parent.bottom
        width: parent.width
        height: 1
        color: "grey"
    }
}
