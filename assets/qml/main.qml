import QtQuick 2.7
import QtQuick.Controls 1.5
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0
import model 1.0
import commands 1.0

ApplicationWindow {
    id: appWindow
    visible: true
    width: 720
    height: 1080
    title: qsTr("Awesome Suduko")

    property string newPuzzleMessage: qsTr("Please create a new puzzle, then select 'Done'")
    property string playModeMessage: qsTr("You can play, solve, or create a new puzzle from the menu above")
    property string solvedPuzzleMessage: qsTr("Puzzle solved!")
    property string notSolvedPuzzleMessage: qsTr("Puzzle could not be solved")


    property bool landscape: width > height
    property CellData chosenCellData;
    property bool newPuzzleMode: false

    /////////////////////
    // Instantiate some commands which will allow us to make calls into c++ code
    // todo: Create these only when we need them

    // This command get the QVariantList of data needed to setup the board
    GetInitialStateCommand {
        id: initialCommand;

        onCompleted: {
           board.model = data
        }
    }

    // This command attempts to solve the puzzle
    SolvePuzzleCommand {
        id: solvePuzzleCommand;

        onCompleted: {
            console.debug("Puzzle solved: " + data)
            progressDialog.hide();

            infoText.text = data ? solvedPuzzleMessage : notSolvedPuzzleMessage
        }
    }

    // This command will save to disk a copy of your game board so you can restore it later
    SavePuzzleCommand {
        id: savePuzzleCommand;

        onCompleted: {
           console.debug("Puzzle saved!")
        }
    }

    Component.onCompleted: {
        initialCommand.execute();
    }

    onNewPuzzleModeChanged: {
        infoText.text = newPuzzleMode ? newPuzzleMessage : playModeMessage
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Create New Puzzle")
                onTriggered: {

                    // clear the board
                    for(var i=0;i<board.model.length;i++){
                        board.model[i].tentativeValue = 0;
                        board.model[i].givenValue = 0;
                    }

                    newPuzzleMode = true;
                }
            }
            MenuItem {
                text: qsTr("Restart Puzzle")
                enabled: !newPuzzleMode
                onTriggered: {
                    for(var i=0;i<board.model.length;i++){
                        board.model[i].tentativeValue = 0;
                    }
                    infoText.text = playModeMessage
                }
            }
            MenuItem {
                text: qsTr("Save Puzzle")
                onTriggered: {
                    savePuzzleCommand.execute(board.model);
                }
            }
            MenuItem {
                text: qsTr("Restore Puzzle")
                onTriggered: {
                    initialCommand.execute();
                }
            }
        }
    }

    Rectangle {
        id: mainItem
        anchors.fill: parent
        color: newPuzzleMode ? "#5516a085" : "#3300A8DF"

        Text {
            id: titleText
            text: newPuzzleMode ? qsTr("Create New Puzzle") : qsTr("Welcome to ") + s_app.applicationName
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: infoText.top
            anchors.bottomMargin: 20
            font.pixelSize : board.width / 20
            font.bold: true
        }

        Text {
            id: infoText
            text: playModeMessage
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: board.top
            anchors.bottomMargin: 20
            width: board.width
            font.pixelSize : board.width / 30
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            color: "grey"

        }

        Board {
            id: board
            width: (landscape ? (parent.height - titleText.contentHeight - infoText.contentHeight - btn.height - 40) : parent.width) * 0.9
            height: width
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            onSelectNumber: {

                // Do not display the selector if they click on a square that cannot be edited
                if(!newPuzzleMode && itemData.givenValue > 0){
                    return;
                }

                chosenCellData = itemData
                numberSelectorDialog.show();
            }
        }

        MyButton {
            id: btn
            width: board.width
            height: width / 9
            anchors.top: board.bottom
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            text: newPuzzleMode ? qsTr("Done") : qsTr("Solve")
            backgroundColor: newPuzzleMode ? "#16a085" : "#00A8DF"

            onClicked: {
                if(newPuzzleMode){
                    newPuzzleMode = false
                }else{
                    progressDialog.show();
                    solvePuzzleCommand.execute(board.model);
                }
            }
        }

        NumberSelectorDialog {

            id: numberSelectorDialog
            anchors.fill: parent

            onNumberSelected: {

               if(newPuzzleMode){
                  chosenCellData.givenValue = number
               }else{
                  chosenCellData.tentativeValue = number
               }
            }
        }

        ProgressDialog {
            id: progressDialog
            anchors.fill: parent
        }

    }


}
