import QtQuick 2.7
import QtQuick.Controls 1.5
import QtQuick.Dialogs 1.2
import model 1.0

Item {

    id: rootItem
    anchors.fill: parent
    property CellData squaredata;

    signal clicked(int number);
    signal cancel();


    MouseArea {
        anchors.fill: parent
        onClicked: {
            rootItem.cancel();
        }
    }

    Rectangle {
        height: (parent.height / 2) + (parent.height / 2) / 4
        width: parent.height / 2
        radius: height / 8
        border.color: "black"
        border.width: 2
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        GridView {
            id: grid
            width: parent.width
            height: width
            cellWidth: parent.width / 3
            cellHeight: cellWidth
            delegate: Component {
                id: contactsDelegate
                NumberItem {
                    number: numberValue
                    onClicked: {
                        rootItem.clicked(numberValue);
                    }
                }
            }
            model: ListModel {
                id: model
                ListElement {numberValue: 1}
                ListElement {numberValue: 2}
                ListElement {numberValue: 3}
                ListElement {numberValue: 4}
                ListElement {numberValue: 5}
                ListElement {numberValue: 6}
                ListElement {numberValue: 7}
                ListElement {numberValue: 8}
                ListElement {numberValue: 9}
            }
            focus: true
            flickableDirection: Flickable.AutoFlickIfNeeded
        }

        Text {
            id: clearText
            text: qsTr("Clear")
            anchors.top: grid.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize : parent.height / 12
        }

        MouseArea {
            anchors.top: grid.bottom
            height: parent.height - grid.height
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            onClicked: {
                rootItem.clicked(0);
            }
        }

    }


}
