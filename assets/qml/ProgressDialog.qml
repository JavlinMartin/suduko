import QtQuick 2.7
import QtQuick.Controls 1.5
import QtQuick.Dialogs 1.2
import model 1.0
import QtGraphicalEffects 1.0

Rectangle{
    id: modal
    anchors.fill: parent
    color: "#66000000"
    opacity: progressDialog.opacity
    visible: false

    MouseArea {
        anchors.fill: parent
        enabled: modal.visible
    }

    function show(){
        modal.visible = true
        numberAnimIn.start();
    }

    function hide(){
        modal.visible = false
        numberAnimOut.start();
    }

    Rectangle {
        id: progressDialog
        height: solvingText.contentHeight + progressBar.height + 50
        width: parent.width * 0.65
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        radius: height / 8
        border.color: "black"
        border.width: 2
        color: "#bb000000";
        visible: false
        opacity: 0

        Text {
            id: solvingText
            text: qsTr("Solving Puzzle ...");
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 20
            color: "#FFFFFF"
            font.pixelSize : parent.width / 20
            font.bold: true
        }

        DropShadow {
            anchors.fill: solvingText
            horizontalOffset: 3
            verticalOffset: 3
            radius: 2
            samples: 5
            color: "#80000000"
            source: solvingText
        }

        ProgressBar {
            id: progressBar
            indeterminate: true
            width: parent.width * 0.8
            height: 10
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: solvingText.bottom
            anchors.topMargin: 10
        }

        ////////////////////////////////
        // Animations

        ParallelAnimation {
            id: numberAnimIn
            NumberAnimation { target: progressDialog; property: "opacity"; from: 0; to: 1; duration: 400; easing.type: Easing.OutBack}
            NumberAnimation { target: progressDialog; property: "scale"; from: 0.5; to: 1; duration: 400; easing.type: Easing.OutBack}

            onStarted: {
                numberAnimOut.stop();
                progressDialog.visible = true;
            }
        }

        ParallelAnimation {
            id: numberAnimOut
            NumberAnimation { target: progressDialog; property: "opacity"; from: 1; to: 0; duration: 150}
            NumberAnimation { target: progressDialog; property: "scale"; from: 1; to: 0.5; duration: 150}

            onStarted: {
                numberAnimIn.stop();
            }

            onStopped: {
                progressDialog.visible = true;
            }
        }
    }


}
