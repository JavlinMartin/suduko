import QtQuick 2.0

Item {
    id: rootRect
    // border.color: "grey"
    // border.width: 1
    height: parent.height / 3
    width: height
    signal clicked();
    property int number : 0;

    Text {
        text: number
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: rootRect.height / 4
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            rootRect.clicked();
        }
    }
}
