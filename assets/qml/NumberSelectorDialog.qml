import QtQuick 2.0
import commands 1.0
import model 1.0
import QtQuick.Layouts 1.3

Rectangle{
    id: modal
    anchors.fill: parent
    color: "#66000000"
    opacity: numberSelector.opacity

    signal numberSelected(int number);

    function show(){
        numberAnimIn.start();
    }

    MouseArea {
        anchors.fill: parent
        enabled: numberSelector.visible
    }

    NumberSelector {
        id: numberSelector
        visible: false
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 0

        onClicked: {
            numberAnimOut.start();
            numberSelected(number);
        }

        onCancel: {
            numberAnimOut.start();
        }
    }

    ////////////////////////////////
    // Animations

    ParallelAnimation {
        id: numberAnimIn
        NumberAnimation { target: numberSelector; property: "opacity"; from: 0; to: 1; duration: 400; easing.type: Easing.OutBack}
        NumberAnimation { target: numberSelector; property: "scale"; from: 0.5; to: 1; duration: 400; easing.type: Easing.OutBack}

        onStarted: {
            numberSelector.visible = true;
        }
    }

    ParallelAnimation {
        id: numberAnimOut
        NumberAnimation { target: numberSelector; property: "opacity"; from: 1; to: 0; duration: 150}
        NumberAnimation { target: numberSelector; property: "scale"; from: 1; to: 0.5; duration: 150}

        onStopped: {
            numberSelector.visible = false;
        }
    }
}


