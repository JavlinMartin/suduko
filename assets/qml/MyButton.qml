import QtQuick 2.4
import model 1.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Button {
    id: btn
    text: ""
    height: s_screen.px(50)
    property real radius: btn.height / 8

    property color backgroundColor: "#00A8DF"

    style: ButtonStyle {

        background: Rectangle {

            implicitWidth: btn.width
            implicitHeight: btn.height
            radius: btn.radius
            color: btn.backgroundColor

            Rectangle {
                implicitWidth: btn.width
                implicitHeight: btn.height
                radius: btn.radius
                visible: control.pressed
                color: btn.backgroundColor
            }
        }

        label: Text {
            text: btn.text
            color: "white"
            font.pixelSize: btn.height / 2
            horizontalAlignment: "AlignHCenter"
            verticalAlignment: "AlignVCenter"
        }
    }
}
