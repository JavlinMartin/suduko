Applicant Name: Mike Martin
Language used: Qt, C++
Tools used: Qt Creator
Platform Support: Android, iOS, Mac, Windows

This Suduko application allows you to solve, play, save, reset, restore and create a new puzzle.

The algorithm to solve the suduko puzzles was created 100% by me (I have not looked at any other solutions). I'm sure there is a more elegant way of solving these puzzles, but I thought I would utilize the power of the processor  ;-)

This cross-platform application was built in Qt 5.7 and leverages its power by deploying the application to 4 platforms. The code for the application itself is shared for the most part among all the platforms, with an 'Application' class for each platform being the exception. This application class doesn't do much, but I thought it was important to show how easy it is to add platform specific code when necessary.

The application is structured in an MVC style approach.
  - Model: "model/CellData.hpp"
  - View: QML Files
  - Controller: Commands

There are 3 commands that I have written for this app, and they all sub-class the base 'Command'.
  1. GetinitialStateCommand: Fires when the application launches or 'restore' is selected
  2. SolvePuzzleCommand: Fires when the user chooses to solve the puzzle
  3. SavePuzzleCommand: Fires when the user chooses to save the puzzle
  
Feel free to peruse the code and read the documentation within for a further description.

/////

Things I would do if I had more time.
  1. More memory management (I'm currently not deleting commands or model objects when done with them)
  2. Flush out the 'Play' mode (Although you can play the game it does not validate or congratulate you when winning)
  3. Add application icon
  4. Fix the Menu on iOS so it's an actual overflow menu
  5. Force portrait mode on iOS, and fix the ui in landscape mode
  5. Unit testing
  6. Error handling
  7. Add cancel button when solving puzzle

///// 

How to Build
  1. Download and install Qt 5.7
  2. Within Qt Creator select 'File->Open File or Project' and choose Suduko.pro
  3. Build and Run the application

////

Dealing with different screen resolutions:

For the most part the UI elements on the screen adjust to the size of the space provided. The application is meant to run in portrait mode. I have managed to force portrait mode on Android, but didn't have the time on iOS. With more time, I may have adjusted the code to have a minimum size for the grid, as I can imagine with very small screens it may get difficult to touch the individual cells.
