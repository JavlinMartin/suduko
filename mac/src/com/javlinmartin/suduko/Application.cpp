//----------------------------------------------------------------------------------------------------------------------
// File:         Application.cpp
// Created on:   2016-11-12
// Author:       Mike Martin
// Description:  A Windows application
//----------------------------------------------------------------------------------------------------------------------

#include "Application.hpp"

namespace javlinmartin {
namespace suduko {

Application::Application(int &argc, char **argv) :
    ApplicationBase(argc, argv) {
    setApplicationName("Mac Suduko");
    init();
}

Application *Application::getInstance() {
    return static_cast<Application *>(QCoreApplication::instance());
}

} // namespace suduko
} // namespace javlinmartin
